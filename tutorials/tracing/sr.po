# Мирослав Николић <miroslavnikolic@rocketmail.com>, 2016–2022.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2021-03-28 20:01+0200\n"
"PO-Revision-Date: 2022-01-05 19:31+0200\n"
"Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>\n"
"Language-Team: Serbian <(nothing)>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Мирослав Николић <miroslavnikolic@rocketmail.com>, 2016–2022."

#. (itstool) path: articleinfo/title
#: tutorial-tracing.xml:6
msgid "Tracing bitmaps"
msgstr "Битмапе прецртавања"

#. (itstool) path: articleinfo/subtitle
#: tutorial-tracing.xml:7
msgid "Tutorial"
msgstr "Упутства"

#. (itstool) path: abstract/para
#: tutorial-tracing.xml:11
msgid ""
"One of the features in Inkscape is a tool for tracing a bitmap image into a "
"&lt;path&gt; element for your SVG drawing. These short notes should help you "
"become acquainted with how it works."
msgstr ""
"Једна од функција у Мастилурку је алат за прецртавање битмап слике у елемент "
"„путање“ за ваше СВГ цртање. Ове кратке белешке треба да вам помогну да се "
"упознате са његовим радом."

#. (itstool) path: article/para
#: tutorial-tracing.xml:16
msgid ""
"Currently Inkscape employs the Potrace bitmap tracing engine (<ulink url="
"\"http://potrace.sourceforge.net\">potrace.sourceforge.net</ulink>) by Peter "
"Selinger. In the future we expect to allow alternate tracing programs; for "
"now, however, this fine tool is more than sufficient for our needs."
msgstr ""
"Тренутно Мастилурко користи Потрејсов погон за прецртавање у битмапу (<ulink "
"url=\"http://potrace.sourceforge.net\">potrace.sourceforge.net</ulink>) који "
"је написао Питер Селинџер. У будућности очекујемо да ћемо омогућити "
"коришћење других програма за прецртавање; за сада, међутим, овај добар алат "
"је више него довољан за наше потребе."

#. (itstool) path: article/para
#: tutorial-tracing.xml:23
msgid ""
"Keep in mind that the Tracer's purpose is not to reproduce an exact "
"duplicate of the original image; nor is it intended to produce a final "
"product. No autotracer can do that. What it does is give you a set of curves "
"which you can use as a resource for your drawing."
msgstr ""
"Имајте на уму да сврха Прецртавача није да репродукује тачан дупликат "
"оригиналне слике; нити је намењен да произведе крајњи производ. Ниједан "
"самостални прецртавач не може то да уради. Већ има за циљ да вам да скуп "
"кривих које можете користити као извориште за ваш цртеж."

#. (itstool) path: article/para
#: tutorial-tracing.xml:28
msgid ""
"Potrace interprets a black and white bitmap, and produces a set of curves. "
"For Potrace, we currently have three types of input filters to convert from "
"the raw image to something that Potrace can use."
msgstr ""
"Потрејс тумачи црно-белу битмапу, и даје скуп кривих. За Потрејс, тренутно "
"имамо три врсте улазних филтера за претварање из сирове слике у нешто што "
"Потрејс може користити."

#. (itstool) path: article/para
#: tutorial-tracing.xml:32
msgid ""
"Generally the more dark pixels in the intermediate bitmap, the more tracing "
"that Potrace will perform. As the amount of tracing increases, more CPU time "
"will be required, and the &lt;path&gt; element will become much larger. It "
"is suggested that the user experiment with lighter intermediate images "
"first, getting gradually darker to get the desired proportion and complexity "
"of the output path."
msgstr ""
"Уопштено, више тамних пиксела у посредничкој битмапи, више прецртавања "
"Потрејс ће обавити. Како се количина прецртавања буде повећавала, биће "
"потребно више времена процесору, а елемент „путање“ ће постати много већи. "
"Саветујемо вам да прво испробате са светлијим посредничким сликама, "
"постепено затамњивати да бисте добили жељену пропорцију и сложеност излазне "
"путање."

#. (itstool) path: article/para
#: tutorial-tracing.xml:38
msgid ""
"To use the tracer, load or import an image, select it, and select the "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Trace Bitmap</guimenuitem></"
"menuchoice> item, or <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"alt\">Alt</keycap><keycap>B</keycap></keycombo>."
msgstr ""
"Да користите прецртавача, учитајте или увезите слику, изаберите је, и "
"изаберите ставку <menuchoice><guimenu>Путања</guimenu><guimenuitem>Векториши "
"битмапу</guimenuitem></menuchoice>, или тастере <keycombo><keycap function="
"\"shift\">Шифт</keycap><keycap function=\"alt\">Алт</keycap><keycap>B</"
"keycap></keycombo>."

#. (itstool) path: article/para
#: tutorial-tracing.xml:50
msgid "The user will see the three filter options available:"
msgstr "Видећете доступне опције три филтера:"

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:55
msgid "Brightness Cutoff"
msgstr "Светлосна граница"

#. (itstool) path: article/para
#: tutorial-tracing.xml:60
msgid ""
"This merely uses the sum of the red, green and blue (or shades of gray) of a "
"pixel as an indicator of whether it should be considered black or white. The "
"threshold can be set from 0.0 (black) to 1.0 (white). The higher the "
"threshold setting, the fewer the number pixels that will be considered to be "
"“white”, and the intermediate image with become darker."
msgstr ""
"Ово само користи збир црвене, зелене и плаве (или нијансе сиве) једног "
"пиксела као показатеља да ли треба бити сматрано црном или белом. Праг се "
"може подесити од 0,0 (црна) до 1,0 (бела). Што је веће подешавање прага, "
"мањи је број пиксела који ће се сматрати „белим“, а посредничка слика ће "
"постати тамнија."

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:75
msgid "Edge Detection"
msgstr "Откривање ивице"

#. (itstool) path: article/para
#: tutorial-tracing.xml:80
msgid ""
"This uses the edge detection algorithm devised by J. Canny as a way of "
"quickly finding isoclines of similar contrast. This will produce an "
"intermediate bitmap that will look less like the original image than does "
"the result of Brightness Threshold, but will likely provide curve "
"information that would otherwise be ignored. The threshold setting here (0.0 "
"– 1.0) adjusts the brightness threshold of whether a pixel adjacent to a "
"contrast edge will be included in the output. This setting can adjust the "
"darkness or thickness of the edge in the output."
msgstr ""
"Ово користи алгоритам за откривање ивице који је осмислио Џ. Кени као начин "
"за брзо проналажење изоклинија сличног контраста. То ће произвести "
"посредничку битмапу која ће изгледати мање као изворна слика него што то ће "
"бити резултат прага осветљења, али ће вероватно пружити информације о кривој "
"које би иначе биле занемарене. Подешавање прага (0,0 – 1,0) дотерује праг "
"осветљења о томе да ли ће пиксел поред ивице контраста бити укључен на "
"излазу. Ово подешавање може подесити зацрњеност или дебљину ивице на излазу."

#. (itstool) path: listitem/para
#: tutorial-tracing.xml:96
msgid "Color Quantization"
msgstr "Засићеност боја"

#. (itstool) path: article/para
#: tutorial-tracing.xml:101
msgid ""
"The result of this filter will produce an intermediate image that is very "
"different from the other two, but is very useful indeed. Instead of showing "
"isoclines of brightness or contrast, this will find edges where colors "
"change, even at equal brightness and contrast. The setting here, Number of "
"Colors, decides how many output colors there would be if the intermediate "
"bitmap were in color. It then decides black/white on whether the color has "
"an even or odd index."
msgstr ""
"Резултат овог филтера ће произвести посредничку слику која се веома "
"разликује од друге две, али је заиста веома корисна. Уместо да покаже "
"изоклиније осветљења или контраста, наћи ће ивице где се боје мењају, чак и "
"при једнакој осветљености и контрасту. Ово подешавање, број боја, одлучује "
"колико излазних боја би било да су посредничке битмапе биле у боји. Онда "
"одлучује црно-бело о томе да ли боја има паран или непаран индекс."

#. (itstool) path: article/para
#: tutorial-tracing.xml:115
msgid ""
"The user should try all three filters, and observe the different types of "
"output for different types of input images. There will always be an image "
"where one works better than the others."
msgstr ""
"Требало би да испробате сва три филтера, и посматрате различите врсте излаза "
"за различите врсте улазних слика. Увек ће постојати једна слика на којој ће "
"неки радити боље од других."

#. (itstool) path: article/para
#: tutorial-tracing.xml:119
msgid ""
"After tracing, it is also suggested that the user try "
"<menuchoice><guimenu>Path</guimenu><guimenuitem>Simplify</guimenuitem></"
"menuchoice> (<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</"
"keycap></keycombo>) on the output path to reduce the number of nodes. This "
"can make the output of Potrace much easier to edit. For example, here is a "
"typical tracing of the Old Man Playing Guitar:"
msgstr ""
"Након прецртавања, такође вам саветујемо да испробате наредбу "
"<menuchoice><guimenu>Путања</guimenu><guimenuitem>Поједностави</"
"guimenuitem></menuchoice> (<keycombo><keycap function=\"control\">Ктрл</"
"keycap><keycap>L</keycap></keycombo>) на излазној путањи да смањите број "
"чворова. Ово може да учини излаз Потрејса много лакшим за уређивање. На "
"пример, испод је типично прецртавање Старца који свира гитару:"

#. (itstool) path: article/para
#: tutorial-tracing.xml:133
msgid ""
"Note the enormous number of nodes in the path. After hitting "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo>, this is a typical result:"
msgstr ""
"Обратите пажњу на огроман број чворова на путањи. Када притиснете "
"<keycombo><keycap function=\"control\">Ктрл</keycap><keycap>L</keycap></"
"keycombo>, ово је типичан резултат:"

#. (itstool) path: article/para
#: tutorial-tracing.xml:144
msgid ""
"The representation is a bit more approximate and rough, but the drawing is "
"much simpler and easier to edit. Keep in mind that what you want is not an "
"exact rendering of the image, but a set of curves that you can use in your "
"drawing."
msgstr ""
"Представљање је мало приближније и грубо, али је цртеж много једноставнији и "
"лакши за уређивање. Имајте на уму да оно што желите није тачно исцртавање "
"слике, већ скуп кривих које можете да користите у цртежу."

#. (itstool) path: Work/format
#: tracing-f01.svg:49 tracing-f02.svg:49 tracing-f03.svg:49 tracing-f04.svg:49
#: tracing-f05.svg:49 tracing-f06.svg:49
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: tracing-f01.svg:70
#, no-wrap
msgid "Main options within the Trace dialog"
msgstr "Главне опције у прозорчету векторисања"

#. (itstool) path: text/tspan
#: tracing-f02.svg:70 tracing-f03.svg:70 tracing-f04.svg:70 tracing-f05.svg:70
#: tracing-f06.svg:70
#, no-wrap
msgid "Original Image"
msgstr "Изворна слика"

#. (itstool) path: text/tspan
#: tracing-f02.svg:81 tracing-f02.svg:97
#, no-wrap
msgid "Brightness Threshold"
msgstr "Померај осветљености"

#. (itstool) path: text/tspan
#: tracing-f02.svg:86 tracing-f03.svg:86 tracing-f04.svg:86
#, no-wrap
msgid "Fill, no Stroke"
msgstr "попуна, без потеза"

#. (itstool) path: text/tspan
#: tracing-f02.svg:102 tracing-f03.svg:102 tracing-f04.svg:102
#, no-wrap
msgid "Stroke, no Fill"
msgstr "потез, без попуне"

#. (itstool) path: text/tspan
#: tracing-f03.svg:81 tracing-f03.svg:97
#, no-wrap
msgid "Edge Detected"
msgstr "Откривена ивица"

#. (itstool) path: text/tspan
#: tracing-f04.svg:81 tracing-f04.svg:97
#, no-wrap
msgid "Quantization (12 colors)"
msgstr "Квантизација (12 боја)"

#. (itstool) path: text/tspan
#: tracing-f05.svg:81
#, no-wrap
msgid "Traced Image / Output Path"
msgstr "Векторисана слика / Излазна путања"

#. (itstool) path: text/tspan
#: tracing-f05.svg:86
#, no-wrap
msgid "(1,551 nodes)"
msgstr "(1551 чвор)"

#. (itstool) path: text/tspan
#: tracing-f06.svg:81
#, no-wrap
msgid "Traced Image / Output Path - Simplified"
msgstr "Векторисана слика / Излазна путања — поједностављено"

#. (itstool) path: text/tspan
#: tracing-f06.svg:86
#, no-wrap
msgid "(384 nodes)"
msgstr "(384 чвора)"
