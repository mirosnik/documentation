# Translators:
# Somogyvári Róbert <cr04ch at gmail>, 2009.
# Arpad Biro <biro.arpad gmail>, 2009.
# Balázs Meskó <meskobalazs@gmail.com>, 2018
# Gyuris Gellért <bubu@ujevangelizacio.hu>, 2018, 2019, 2020
#
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Elements of Design\n"
"POT-Creation-Date: 2021-03-19 18:35+0100\n"
"PO-Revision-Date: 2020-04-3 23:31+0000\n"
"Last-Translator: Gyuris Gellért <bubu@ujevangelizacio.hu>, 2020\n"
"Language-Team: Hungarian (https://www.transifex.com/fsf-hu/teams/77907/hu/)\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: elements-f15.svg:452(format) elements-f14.svg:173(format)
#: elements-f13.svg:102(format) elements-f12.svg:445(format)
#: elements-f11.svg:48(format) elements-f10.svg:48(format)
#: elements-f09.svg:48(format) elements-f08.svg:206(format)
#: elements-f07.svg:91(format) elements-f06.svg:91(format)
#: elements-f05.svg:48(format) elements-f04.svg:48(format)
#: elements-f03.svg:48(format) elements-f02.svg:48(format)
#: elements-f01.svg:48(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: elements-f12.svg:710(tspan)
#, no-wrap
msgid "Random Ant &amp; 4WD"
msgstr "Random Ant &amp; 4WD"

#: elements-f12.svg:721(tspan)
#, no-wrap
msgid "SVG  Image Created by Andrew Fitzsimon"
msgstr "Andrew Fitzsimon SVG-rajza"

#: elements-f12.svg:726(tspan)
#, no-wrap
msgid "Courtesy of Open Clip Art Library"
msgstr "Az Open Clip Art Library szívességéből"

#: elements-f12.svg:731(tspan)
#, no-wrap
msgid "http://www.openclipart.org/"
msgstr "http://www.openclipart.org/"

#: elements-f04.svg:79(tspan)
#, no-wrap
msgid "BIG"
msgstr "NAGY"

#: elements-f04.svg:93(tspan)
#, no-wrap
msgid "small"
msgstr "kicsi"

#: elements-f01.svg:88(tspan)
#, no-wrap
msgid "Elements"
msgstr "Elemek"

#: elements-f01.svg:101(tspan)
#, no-wrap
msgid "Principles"
msgstr "Alapelvek"

#: elements-f01.svg:114(tspan) tutorial-elements.xml:89(title)
#, no-wrap
msgid "Color"
msgstr "Szín"

#: elements-f01.svg:127(tspan) tutorial-elements.xml:30(title)
#, no-wrap
msgid "Line"
msgstr "Vonal"

#: elements-f01.svg:140(tspan) tutorial-elements.xml:45(title)
#, no-wrap
msgid "Shape"
msgstr "Forma"

#: elements-f01.svg:153(tspan) tutorial-elements.xml:74(title)
#, no-wrap
msgid "Space"
msgstr "Tér"

#: elements-f01.svg:166(tspan) tutorial-elements.xml:104(title)
#, no-wrap
msgid "Texture"
msgstr "Textúra"

#: elements-f01.svg:179(tspan) tutorial-elements.xml:118(title)
#, no-wrap
msgid "Value"
msgstr "Érték"

#: elements-f01.svg:192(tspan) tutorial-elements.xml:60(title)
#, no-wrap
msgid "Size"
msgstr "Méret"

#: elements-f01.svg:205(tspan) tutorial-elements.xml:138(title)
#, no-wrap
msgid "Balance"
msgstr "Egyensúly"

#: elements-f01.svg:218(tspan) tutorial-elements.xml:153(title)
#, no-wrap
msgid "Contrast"
msgstr "Kontraszt"

#: elements-f01.svg:231(tspan) tutorial-elements.xml:166(title)
#, no-wrap
msgid "Emphasis"
msgstr "Hangsúly"

#: elements-f01.svg:244(tspan) tutorial-elements.xml:180(title)
#, no-wrap
msgid "Proportion"
msgstr "Arány"

#: elements-f01.svg:257(tspan) tutorial-elements.xml:193(title)
#, no-wrap
msgid "Pattern"
msgstr "Minta"

#: elements-f01.svg:271(tspan) tutorial-elements.xml:206(title)
#, no-wrap
msgid "Gradation"
msgstr "Átmenet"

#: elements-f01.svg:284(tspan) tutorial-elements.xml:222(title)
#, no-wrap
msgid "Composition"
msgstr "Kompozíció"

#: elements-f01.svg:297(tspan)
#, no-wrap
msgid "Overview"
msgstr "Áttekintés"

#: tutorial-elements.xml:6(title)
msgid "Elements of design"
msgstr "A tervezés elemei"

#: tutorial-elements.xml:7(subtitle)
msgid "Tutorial"
msgstr "Ismertető"

#: tutorial-elements.xml:11(para)
msgid ""
"This tutorial will demonstrate the elements and principles of design which "
"are normally taught to early art students in order to understand various "
"properties used in art making. This is not an exhaustive list, so please "
"add, subtract, and combine to make this tutorial more comprehensive."
msgstr ""
"Az alábbiakban a grafikai tervezés azon elemeit és alapelveit mutatjuk be, "
"melyeket a kezdő művészeti hallgatóknak oktatnak azért, hogy megismertessék "
"velük a művészi rajz különböző sajátságait. Nem egy mindent részletező, "
"teljes listát nyújtunk, Ön is nyugodtan bővítheti vagy javíthatja, hogy még "
"átfogóbbá tegye ezt az ismertetőt."

#: tutorial-elements.xml:25(title)
msgid "Elements of Design"
msgstr "A tervezés elemei"

#: tutorial-elements.xml:26(para)
msgid "The following elements are the building blocks of design."
msgstr "A következő elemek a grafikai tervezés építőkockái:"

#: tutorial-elements.xml:31(para)
msgid ""
"A line is defined as a mark with length and direction, created by a point "
"that moves across a surface. A line can vary in length, width, direction, "
"curvature, and color. Line can be two-dimensional (a pencil line on paper), "
"or implied three-dimensional."
msgstr ""
"A vonal az a hosszal és iránnyal bíró jel, amelyet egy felületen végigmozgó "
"pont hoz létre. A vonalnak változhat a hossza, a szélessége, az iránya, a "
"görbülete és a színe. A vonal lehet sík- (egy tollvonás a papíron) vagy "
"térhatású."

#: tutorial-elements.xml:46(para)
msgid ""
"A flat figure, shape is created when actual or implied lines meet to "
"surround a space. A change in color or shading can define a shape. Shapes "
"can be divided into several types: geometric (square, triangle, circle) and "
"organic (irregular in outline)."
msgstr ""
"A forma egy határozott alakzat, mely azáltal jön létre, hogy tényleges vagy "
"érzékeltetett vonalak találkozása körülzár egy teret. A szín vagy árnyék "
"változása is meghatározhat egy formát. A formák különféle típusokba "
"sorolhatók: geometriai (négyszög, háromszög, kör) és organikus "
"(szabálytalanságok a körvonalban)."

#: tutorial-elements.xml:61(para)
msgid ""
"This refers to variations in the proportions of objects, lines or shapes. "
"There is a variation of sizes in objects either real or imagined."
msgstr ""
"A méret az objektumok, vonalak vagy formák nagyságbeli váltakozásaira utal. "
"A méretek különbözősége lehet valóságos vagy látszólagos."

#: tutorial-elements.xml:75(para)
msgid ""
"Space is the empty or open area between, around, above, below, or within "
"objects. Shapes and forms are made by the space around and within them. "
"Space is often called three-dimensional or two- dimensional. Positive space "
"is filled by a shape or form. Negative space surrounds a shape or form."
msgstr ""
"A tér egy üres vagy nyitott terület az objektumok között, körül, felett, "
"alatt vagy az objektumok belsejében. Az alakok és formák a körülöttük és "
"bennük levő tér által jönnek létre. A teret gyakran nevezik sík- vagy "
"térhatásúnak. Egy pozitív teret valamilyen alakzat vagy forma tölt ki. Egy "
"negatív teret valamilyen forma vagy alakzat zár körül."

#: tutorial-elements.xml:90(para)
msgid ""
"Color is the perceived character of a surface according to the wavelength of "
"light reflected from it. Color has three dimensions: HUE (another word for "
"color, indicated by its name such as red or yellow), VALUE (its lightness or "
"darkness), INTENSITY (its brightness or dullness)."
msgstr ""
"A szín a felületről visszaverődő fény hullámhosszának változása által "
"érzékelhető tulajdonság. A szín három mértéke: a SZÍNÁRNYALAT (vagyis a szín "
"neve, mint a piros vagy sárga), az ÉRTÉK (a világosság vagy sötétség) és az "
"INTENZITÁS (a fényesség vagy homályosság)."

#: tutorial-elements.xml:105(para)
msgid ""
"Texture is the way a surface feels (actual texture) or how it may look "
"(implied texture). Textures are described by word such as rough, silky, or "
"pebbly."
msgstr ""
"A textúra egy felület tapintható (valóságos textúra) vagy látható "
"(látszólagos textúra) anyagi tulajdonsága. A textúra olyan szavakkal írható "
"le, mint érdes, selymes vagy göröngyös."

#: tutorial-elements.xml:119(para)
msgid ""
"Value is how dark or how light something looks. We achieve value changes in "
"color by adding black or white to the color. Chiaroscuro uses value in "
"drawing by dramatically contrasting lights and darks in a composition."
msgstr ""
"Az értéktől függ, hogy mennyire sötétnek vagy világosnak látszik valami. Az "
"érték változása a színhez adott feketével vagy fehérrel érhető el. A "
"chiaroscuro az értéket használja a fény és az árnyék kompozíción belüli, "
"drámai kontrasztjához."

#: tutorial-elements.xml:133(title)
msgid "Principles of Design"
msgstr "Alapelvek"

#: tutorial-elements.xml:134(para)
msgid "The principles use the elements of design to create a composition."
msgstr ""
"Az elemekből az alapelveket szem előtt tartva alkotható meg a kompozíció."

#: tutorial-elements.xml:139(para)
msgid ""
"Balance is a feeling of visual equality in shape, form, value, color, etc. "
"Balance can be symmetrical or evenly balanced or asymmetrical and un-evenly "
"balanced. Objects, values, colors, textures, shapes, forms, etc., can be "
"used in creating a balance in a composition."
msgstr ""
"Az egyensúly az alakok, formák, értékek, színek stb. vizuális "
"egyöntetűségének érzetét jelenti. Az egyensúly lehet szimmetrikus vagy "
"egyenletes, illetve aszimmetrikus vagy nem egyenletes. A kompozíción belüli "
"egyensúly az objektumok, értékek, színek, textúrák, alakok, formák stb. "
"által érhető el."

#: tutorial-elements.xml:154(para)
msgid "Contrast is the juxtaposition of opposing elements"
msgstr ""
"A kontraszt egymástól nagyon különböző vagy éppen egymásnak ellentmondó "
"elemek egymás mellé helyezése által jön létre."

#: tutorial-elements.xml:167(para)
msgid ""
"Emphasis is used to make certain parts of their artwork stand out and grab "
"your attention. The center of interest or focal point is the place a work "
"draws your eye to first."
msgstr ""
"A hangsúly a műalkotás bizonyos részeinek kiemelésére és a figyelem "
"megragadására szolgál. Egy rajzon a tekintet először mindig egy érdekes "
"pontra téved."

#: tutorial-elements.xml:181(para)
msgid ""
"Proportion describes the size, location or amount of one thing compared to "
"another."
msgstr ""
"Az arány a méret, terület vagy valamilyen dolog mennyiségét egy másikéval "
"összehasonlítva leíró jellemző."

#: tutorial-elements.xml:194(para)
msgid ""
"Pattern is created by repeating an element (line, shape or color) over and "
"over again."
msgstr ""
"A minta egy elem (vonal, forma vagy szín) újra és újra ismétlésével hozható "
"létre."

#: tutorial-elements.xml:207(para)
msgid ""
"Gradation of size and direction produce linear perspective. Gradation of "
"color from warm to cool and tone from dark to light produce aerial "
"perspective. Gradation can add interest and movement to a shape. A gradation "
"from dark to light will cause the eye to move along a shape."
msgstr ""
"A méret és az irány átmenete vonaltávlatot, míg a szín átmenete melegből "
"hidegbe és a tónus átmenete sötétből világosba légtávlatot eredményez. Az "
"átmenet alkalmas a figyelem felkeltésére, és egy alak mozgását is "
"érzékeltetheti. A sötétből a világosba való átmenet hatására a szem végigfut "
"az alakon."

#: tutorial-elements.xml:223(para)
msgid "The combining of distinct elements to form a whole."
msgstr ""
"Különböző elemek teljes egésszé szerkesztésével alkotható meg a kompozíció."

#: tutorial-elements.xml:235(title)
msgid "Bibliography"
msgstr "Irodalomjegyzék"

#: tutorial-elements.xml:236(para)
msgid "This is a partial bibliography used to build this document."
msgstr ""
"Részleges jegyzék az e dokumentum készítéséhez felhasznált irodalomról."

#: tutorial-elements.xml:242(ulink)
msgid "http://www.makart.com/resources/artclass/EPlist.html"
msgstr "http://www.makart.com/resources/artclass/EPlist.html"

#: tutorial-elements.xml:247(ulink)
msgid "http://www.princetonol.com/groups/iad/Files/elements2.htm"
msgstr "http://www.princetonol.com/groups/iad/Files/elements2.htm"

#: tutorial-elements.xml:252(ulink)
msgid "http://www.johnlovett.com/test.htm"
msgstr "http://www.johnlovett.com/test.htm"

#: tutorial-elements.xml:257(ulink)
msgid "http://digital-web.com/articles/elements_of_design/"
msgstr "http://digital-web.com/articles/elements_of_design/"

#: tutorial-elements.xml:262(ulink)
msgid "http://digital-web.com/articles/principles_of_design/"
msgstr "http://digital-web.com/articles/principles_of_design/"

#: tutorial-elements.xml:266(para)
msgid ""
"Special thanks to Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/"
"\">http://www.coroflot.com/redlucite/</ulink>) for helping me (<ulink url="
"\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) with this tutorial. "
"Also, thanks to the Open Clip Art Library (<ulink url=\"http://www."
"openclipart.org/\">http://www.openclipart.org/</ulink>) and the graphics "
"people have submitted to that project."
msgstr ""
"Külön köszönet Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/"
"\">http://www.coroflot.com/redlucite</ulink>/) segítségéért, melyet az "
"ismertető elkészítésében nyújtott nekem (<ulink url=\"http://www.rejon.org/"
"\">http://www.rejon.org/</ulink>). Szintén köszönetet az Open Clip Art "
"Librarynek (<ulink url=\"http://www.openclipart.org/\">http://www."
"openclipart.org/</ulink>) és a munkáikkal őket támogató grafikusoknak."

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-elements.xml:0(None)
msgid "translator-credits"
msgstr ""
"Somogyvári Róbert <cr04ch at gmail>, 2008\n"
"Gyuris Gellért <bubu@ujevangelizacio.hu>, 2018, 2019, 2020"
